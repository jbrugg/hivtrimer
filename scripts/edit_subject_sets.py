#!/usr/bin/python

import requests
from rauth import OAuth2Service

import argparse
import csv
import json
import sys
import random
import string

import panoptes

def main():

    my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
    my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

    panoptes_auth = OAuth2Service(
        client_id=my_id,
        client_secret=my_secret,
        name='panoptes',
        authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
        access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
        base_url="https://panoptes.zooniverse.org/"
        )

    params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}
    
    r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

    token = r.json()["access_token"]

    panoptes_projs = []

    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/panoptes_links.tsv','r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')

        for line in reader:
            panoptes_projs.append(line)

    # entry = project_id,workflow_id,set_id

    out_data = []

    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/img_lists/trimer_list_01.txt','r') as in_file:

        for line in in_file:
            print line
            proj_info = panoptes_projs.pop()
            
            line = line.strip('\n')

            files = ["/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/red_cyan_trimers/flip_%s" % line,
                     "/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/trimer_pngs/flip_%s" % line,
                     "/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/trimer_pngs/%s" % line]

            upload_status,subj_id,code = panoptes.upload_subjects(token,files,proj_info[0])
            if upload_status != 201:
                print line, upload_status
                return

            change_status = panoptes.edit_subject_sets(token,subj_id,proj_info[2])

            entry = [code,subj_id,proj_info[0],proj_info[1],proj_info[2],line]

            out_data.append(entry)


    

    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/manifestos/timer_01.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')

        for line in out_data:
            writer.writerow(line)

    return

if __name__ == "__main__":
    main()
