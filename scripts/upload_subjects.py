#!/usr/bin/python

import requests
from rauth import OAuth2Service

import argparse
import csv
import json
import sys
import random
import string
import os

def main():

    parser = argparse.ArgumentParser(description='Upload your subjects to Panoptes!')
    parser.add_argument('img_file',metavar='input',type=str,help='Tab-delimited text file that contains image files to upload (must be png)')

    args = parser.parse_args()

    img_files = []

    with open(args.img_file,'r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')
        for line in reader:
            img_files.append(line)

    my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
    my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

    panoptes = OAuth2Service(
        client_id=my_id,
        client_secret=my_secret,
        name='panoptes',
        authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
        access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
        base_url="https://panoptes.zooniverse.org/"
        )

    params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}
    
    r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

    token = r.json()["access_token"]

    header_get = {'Accept': 'application/vnd.api+json; version=1',
                  'Content-Type': 'application/json',
                  'Authorization' : 'Bearer %s' % token}

    put_dat = []

    for files in img_files:

        img_code = ''.join(random.choice(string.ascii_uppercase) for _ in range(8))
        
        payload = {
            "subjects": {
                "locations": [
                    "image/png",
                    "image/png",
                    ],
                "metadata": {
                    "code": img_code
                    },
                "links": {
                    "project": "1893"
                    }
                }
            }

        
        post_req = requests.post("https://panoptes.zooniverse.org/api/subjects",headers=header_get,data=json.dumps(payload))
        post_reply = post_req.text

        post_json = post_req.json()
        
        subj_id = post_json['subjects'][0]['id']
        urls = map(lambda x: x['image/png'], post_json['subjects'][0]['locations'])
        
        for i in range(len(urls)):
            url = urls[i]

            header_put = {'Content-Type': 'image/png'}
            
            with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/trimer_pngs/%s' % files[i],'rb') as f:
                put_req = requests.put(url,headers=header_put,data=f)
        
        put_dat.append([subj_id,img_code,files[0]])
                
    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/logs/put_test.txt','w') as out_file:
        out_file.write('%s\t' % subj_id)
        out_file.write('%s\n' % files[0])



    return


if __name__ == "__main__":
    main()
