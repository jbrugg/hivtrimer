#!/usr/bin/python

from boto.mturk.connection import MTurkConnection
from boto.mturk.question import QuestionContent,Question,QuestionForm,Overview,AnswerSpecification,SelectionAnswer,FormattedContent,FreeTextAnswer
from rauth import OAuth2Service
import requests
import urllib,urllib2
import csv
import copy
import time

import sys
import json
import os

def check_HIT(line,req_header,class_data):

    user_name = line[0]
    subject_id = line[2]
    
    user_data = []

    # for x in  class_data:
    #     print x[3]
    #     print x[3].split(';')

    class_data = filter(lambda x: (x[2] == user_name) & (x[3].split(';')[0] == subject_id),class_data)

    for anno in class_data:

        class_id = anno[0]
        anno = anno[1]

        marks = map(lambda x: [x['x'],x['y']], anno['value'])
        user_data.append((class_id,marks))

    user_data = sorted(user_data,key=lambda x: len(x[1]))

    if user_data:
        longest_class = user_data[-1]

        line.append(longest_class[0])
        line.append(len(longest_class[1]))
        line.append(len(user_data))
        

        with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/acceptions.tsv','aw') as out_file:
            tsv_writer = csv.writer(out_file,delimiter='\t')
            tsv_writer.writerow(line)

        if not os.path.exists('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/annotations/%s/' % subject_id):
            os.makedirs('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/annotations/%s' % subject_id)

        with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/annotations/%s/%s.tsv' % (subject_id,longest_class[0]),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            out_file.write('%s\n' % user_name)
            for mark in longest_class[1]: writer.writerow(mark)


        return True

    else:

        with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/rejections.tsv','aw') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(line)

        return False


def main():

    ACCESS_ID = 'AKIAISFIFOVDTUA47Z7Q'
    SECRET_KEY = '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'
    HOST = 'mechanicalturk.amazonaws.com'
    # HOST = 'mechanicalturk.sandbox.amazonaws.com'

    mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                          aws_secret_access_key=SECRET_KEY,
                          host=HOST)

    my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
    my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

    panoptes = OAuth2Service(
        client_id=my_id,
        client_secret=my_secret,
        name='panoptes',
        authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
        access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
        base_url="https://panoptes.zooniverse.org/"
        )

    params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}
    
    r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

    token = r.json()["access_token"]

    header_get = {'Accept': 'application/vnd.api+json; version=1',
                  'Content-Type': 'application/json',
                  'Authorization' : 'Bearer %s' % token}

    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/proj_data.csv','r') as in_file:
        reader = csv.reader(in_file,delimiter=',',quotechar='"')
        class_data = [[line[0],line[-3],line[1],line[-1]] for line in reader] #class_id, annotation, user_name, subj_id

    class_data = map(lambda x: [x[0],json.loads(x[1][1:-1]),x[2],x[3]],class_data)

    os.remove('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/rejections.tsv')

    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/assignments_2_verify.tmp','r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')
        for line in reader:
            results = check_HIT(line,header_get,class_data)
            print results
            
            if results:
                mtc.approve_assignment(line[1])


if __name__ == "__main__":
    main()

