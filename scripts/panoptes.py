#!/usr/bin/python

import requests
import csv
import json
import random
import string

def create_subject_set(token,subj_ids,set_name):

    header_post = {'Accept': 'application/vnd.api+json; version=1',
              'Content-Type': 'application/json',
              'Authorization' : 'Bearer %s' % token}


    payload = {
        "subject_sets": {
            "display_name": set_name,
            "links": {
                "project": "1457",
                "subjects": subj_ids
            }
        }
    }

    requests.post("https://panoptes.zooniverse.org/api/subject_sets",headers=header_post,data=json.dumps(payload))

    return

def upload_subjects(token,img_files,proj_id):

    header_get = {'Accept': 'application/vnd.api+json; version=1',
                  'Content-Type': 'application/json',
                  'Authorization' : 'Bearer %s' % token}

    img_code = ''.join(random.choice(string.ascii_uppercase) for _ in range(8))

    payload = {
        "subjects": {
            "locations": [
                "image/png",
                "image/png",
                "image/png"
                ],
            "metadata": {
                "code": img_code
                },
            "links": {
                "project": "%s" % proj_id
                }
            }
        }


    post_req = requests.post("https://panoptes.zooniverse.org/api/subjects",headers=header_get,data=json.dumps(payload))
    post_reply = post_req.text

    post_json = post_req.json()

    subj_id = post_json['subjects'][0]['id']
    urls = map(lambda x: x['image/png'], post_json['subjects'][0]['locations'])

    for i in range(len(urls)):
        url = urls[i]
        
        header_put = {'Content-Type': 'image/png'}
        
        with open(img_files[i],'rb') as f:
            put_req = requests.put(url,headers=header_put,data=f)
            
    return post_req.status_code,subj_id,img_code


def edit_subject_sets(token,subj_id,set_id):

    header_get = {'Accept': 'application/vnd.api+json; version=1',
                  'Content-Type': 'application/json',
                  'Authorization' : 'Bearer %s' % token}

    subjects_get = requests.get("https://panoptes.zooniverse.org/api/subjects?subject_set_id=%s" % set_id,headers=header_get)

    subject_ids = map(lambda x: x['id'], subjects_get.json()['subjects'])

    for id in subject_ids:
        delete_req = requests.delete('https://panoptes.zooniverse.org/api/subject_sets/%s/links/subjects/%s' % (set_id,id),headers=header_get)


    payload = {
        "subjects": [
            subj_id
            ]
        }

    post_req = requests.post('https://panoptes.zooniverse.org/api/subject_sets/%s/links/subjects' % set_id,headers=header_get,data=json.dumps(payload))

    return post_req.status_code

