#!/usr/bin/python

import requests
from rauth import OAuth2Service

import argparse
import csv
import json
import sys
import random
import string


def main():

    my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
    my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

    panoptes_auth = OAuth2Service(
        client_id=my_id,
        client_secret=my_secret,
        name='panoptes',
        authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
        access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
        base_url="https://panoptes.zooniverse.org/"
        )

    params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}
    
    r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

    token = r.json()["access_token"]

    header_post = {'Accept': 'application/vnd.api+json; version=1',
              'Content-Type': 'application/json',
              'Authorization' : 'Bearer %s' % token}


    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/manifestos/timer_01.tsv','r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')

        subj_ids = map(lambda x: x[1], reader)

    payload = {
        "subject_sets": {
            "display_name": "HIV Trimer cyan-red",
            "links": {
                "project": "1457",
                "subjects": subj_ids
            }
        }
    }

    requests.post("https://panoptes.zooniverse.org/api/subject_sets",headers=header_post,data=json.dumps(payload))

if __name__ == '__main__':
    main()
