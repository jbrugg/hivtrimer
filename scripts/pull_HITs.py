#!/usr/bin/python

from boto.mturk.connection import MTurkConnection
from boto.mturk.question import QuestionContent,Question,QuestionForm,Overview,AnswerSpecification,SelectionAnswer,FormattedContent,FreeTextAnswer
from rauth import OAuth2Service
import requests
import urllib,urllib2
import csv
import copy
import time

import sys
import json

def main():

    ACCESS_ID = 'AKIAISFIFOVDTUA47Z7Q'
    SECRET_KEY = '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'
    HOST = 'mechanicalturk.amazonaws.com'
    # HOST = 'mechanicalturk.sandbox.amazonaws.com'

    mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                          aws_secret_access_key=SECRET_KEY,
                          host=HOST)

    hits = mtc.get_all_hits()

    code_dict = {}
    
    with open("/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/manifestos/timer_01.tsv",'r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')
        for line in reader:
            code_dict[line[0]] = line[1:]
        
    out_data = []
    rej_data = []

    for hit in hits:

        hit_id = hit.HITId

        hit_ass = mtc.get_assignments(hit_id)
        
        for ass in hit_ass:
            
            status = ass.AssignmentStatus
            
            ass_id = ass.AssignmentId

            submit_time = ass.SubmitTime

            if status == 'Submitted':

                entry = [copy.deepcopy(ass.WorkerId)]
                
                entry.append(ass_id)
                
                answer_form = ass.answers[0][0]
                answers = answer_form.fields[0]
                answer_vec = answers.split('|')
		answer = answer_vec[0].strip(' ').strip('\t')
                
		work_info = code_dict.get(answer,'ERROR')
		if work_info == "ERROR":
			entry.append(answer)
			rej_data.append(entry)
		else:
			entry.extend(work_info)
                        entry.append(submit_time)
			out_data.append(entry)


    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/assignments_2_verify.tmp','w') as out_file:
        dat_writer = csv.writer(out_file,delimiter='\t')
        for line in out_data:
            dat_writer.writerow(line)

    with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/hit_assignments/'+time.strftime("%H-%M-%S-%d-%m-%Y")+"_assignment.tsv",'w') as out_file:
        dat_writer = csv.writer(out_file,delimiter='\t')
        for line in out_data:
            dat_writer.writerow(line)
    

    rejection_file = '/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/hit_rejections/'+time.strftime("%H-%M-%S-%d-%m-%Y")+"_rejection.tsv"
    with open(rejection_file,'w') as out_file:
	dat_writer = csv.writer(out_file,delimiter='\t')
	for line in rej_data:
		dat_writer.writerow(line)

    return

if __name__ == '__main__':
    main()

