#!/usr/bin/python

from PIL import Image, ImageDraw
import os
import itertools
import random

def main():

    colors = [(0,0,0),          # black   0
              (255,255,255),    # white   1
              (255,0,0),        # red     2
              (0,255,0),        # lime    3
              (0,0,255),        # blue    4
              (255,255,0),      # yellow  5
              (0,255,255),      # cyan    6
              (255,0,255),      # magenta 7
              (128,128,128),    # gray    8
              (128,0,0),        # maroon  9
              (128,128,0),      # olive   10
              (0,128,0),        # green   11
              (128,0,128),      # purple  12
              (0,128,128),      # teal    13
              (0,0,128)]        # navy    14

    color_names = ['black',
                   'white',
                   'red',
                   'lime',
                   'blue',
                   'yellow',
                   'cyan',
                   'magenta',
                   'gray',
                   'maroon',
                   'olive',
                   'green',
                   'purple',
                   'teal',
                   'navy']

    acceptable_combos = [(0,6),
                         (0,3),
                         (0,1),
                         (0,5),
                         (4,5),
                         (6,0),
                         (6,4),
                         (6,7),
                         (6,9),
                         (6,14),
                         (6,12),
                         (6,2),
                         (11,2),
                         (11,1),
                         (11,5),
                         (3,0),
                         (3,4),
                         (3,7),
                         (3,9),
                         (3,14),
                         (3,12),
                         (3,2),
                         (7,6),
                         (7,11),
                         (7,14),
                         (7,13),
                         (9,6),
                         (9,3),
                         (9,5),
                         (14,6),
                         (14,3),
                         (14,1),
                         (14,5),
                         (10,14),
                         (12,6),
                         (12,3),
                         (12,1),
                         (12,5),
                         (2,0),
                         (2,6),
                         (2,3),
                         (2,14),
                         (13,5),
                         (1,0),
                         (1,4),
                         (1,14),
                         (5,4),
                         (5,7),
                         (5,9),
                         (5,14)]
                         

    for img_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/trimer_8_pngs/'):

        # # img = Image.open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/test_8.png' ,'r')
        # img = Image.open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/lid_test_8.png','r')

        img = Image.open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/trimer_8_pngs/%s' % img_file,'r')

        gray_img = img.convert(mode='RGB')
        
        pix_vals = list(img.getdata())

        # cnt = 0

        # for comb in itertools.permutations(xrange(len(colors)),2):
        #     source = colors[comb[0]]
        #     dest = colors[comb[1]]

        #     slope = ((dest[0]*1.0-source[0])/255,(dest[1]*1.0-source[1])/255,(dest[2]*1.0-source[2])/255)

        #     new_vals = map(lambda i: (int(source[0]+i*slope[0]),int(source[1]+i*slope[1]),int(source[2]+i*slope[2])),pix_vals)

        #     gray_img.putdata(new_vals)
        #     gray_img.save('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/tests/test_%s_%s.png' % (color_names[comb[0]],color_names[comb[1]]))

        #     cnt += 1

        random.shuffle(acceptable_combos)
        comb = acceptable_combos[0]
        # source = colors[comb[1]]
        # dest = colors[comb[0]]

        dest = (0,255,255)
        source = (255,0,0)

        slope = ((dest[0]*1.0-source[0])/255,(dest[1]*1.0-source[1])/255,(dest[2]*1.0-source[2])/255)

        new_vals = map(lambda i: (int(source[0]+i*slope[0]),int(source[1]+i*slope[1]),int(source[2]+i*slope[2])),pix_vals)

        gray_img.putdata(new_vals)
        gray_img.save('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/data/red_cyan_trimers/%s' % img_file)

        
    return

if __name__ == "__main__":
    main()
