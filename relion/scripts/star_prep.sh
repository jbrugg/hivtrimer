#!/bin/bash

rm -f /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/star_prep.tsv

for vote_file in `ls /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/vote_files/`
do
    awk -v file_name=$vote_file '{print file_name,$1,$2}' OFS="\t" /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/vote_files/$vote_file >> /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/star_prep.tsv

done

sed -e 's/vote_//g' -e 's/.tsv//g' /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/star_prep.tsv > /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo
mv /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/star_prep.tsv

python /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/scripts/id2img.py
sed 's/.jpg//g' /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo_init.tsv > /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/star_init.tsv
#mv /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/star_init.tsv

## AMT side

rm -f /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/amt_star_prep.tsv

for vote_file in `ls /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/vote_files/`
do
    awk -v file_name=$vote_file '{print file_name,$1,$2}' OFS="\t" /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/user_results/vote_files/$vote_file >> /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/amt_star_prep.tsv

done

sed -e 's/vote_//g' -e 's/.tsv//g' /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/amt_star_prep.tsv > /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo
mv /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/amt_star_prep.tsv

python /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/scripts/id2img.py
sed 's/.jpg//g' /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo_init.tsv > /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/amt_star_init.tsv
#mv /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/foo /Users/Jake/Documents/Projects/crowdProj/HIVtrimer/relion/data/amt_star_init.tsv
