#!/usr/bin/python

import csv
import json
import os

summary_data = []

with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/raw_data/trimer_data.csv','r') as in_file:
    reader = csv.reader(in_file,delimiter=',')
    
    for line in reader:
        
        user_id = line[1]
        class_id = line[0]
        
        workflow_version = line[6]
        
        annotation = json.loads(line[11])
        
        anno = annotation[0]

        marks = map(lambda x: [x['x'],x['y']],anno['value'])

        subject = json.loads(line[12])
        subj_id = subject.keys()[0]

        summary_data.append([subj_id,class_id,user_id,len(marks),workflow_version,line[7]])

        if not os.path.exists('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/user_annos/%s' % subj_id):
            os.makedirs('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/user_annos/%s' % subj_id)

        with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/user_annos/%s/%s.tsv' % (subj_id,class_id),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            out_file.write('%s\n' % user_id)
            out_file.write('%s\n' % workflow_version)
            for mark in marks: writer.writerow(mark)


with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/analysis/crowd_summary.tsv','w') as out_file:

    writer = csv.writer(out_file,delimiter='\t')

    writer.writerow(['subj_id','class_id','user_id','anno_len','work_version','time'])
    for line in summary_data:
        writer.writerow(line)
