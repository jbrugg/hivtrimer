#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num


def main():

    anno_data = {}

    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/user_annos/'):
        subj_entry = []
        
        for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/user_annos/%s/' % subj_dir):
            with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                reader = csv.reader(in_file,delimiter='\t')
                entry = []
                
                reader.next() #user_id
                reader.next() #version
                
                for line in reader:
                    conv_line = map(lambda x: float(x), line)
                    entry.append(my_point(conv_line,1))

            subj_entry.append(entry)
        anno_data[subj_dir] = subj_entry


    for subj_id,marks in anno_data.iteritems():
        points = n_vote(copy.deepcopy(marks),10)

        with open('/Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/annotations/vote_files/vote_%s.tsv' % subj_id,'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')

            for mark in points:
                writer.writerow([mark.coord[0],mark.coord[1],mark.num])

    return


def gold_eval(datapts,goldpts):
    
    #let's do this by a total evaluation... like look over the whole corpus. 

    agreement = {}

    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 or exp_n == 0:
        #return (1.0,1.0,1.0)
        return (0,0,0)

    #print goldpts
    #print datapts
         
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    return (tp,fp,fn)


def find_matches(mat,fewer_rows,dist_thresh):
    #rows
    row_mins = []
    for i in xrange(mat.shape[0]):
        inds = np.where(mat[i,:] == min(mat[i,:]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        row_mins.append(ind)

    col_mins = []
    for i in xrange(mat.shape[1]):
        inds = np.where(mat[:,i] == min(mat[:,i]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        col_mins.append(ind)


    matches = []

    if fewer_rows:
        for i in xrange(len(row_mins)):
            col_ind = row_mins[i]
            if mat[i,col_ind] < dist_thresh:
                matches.append((i,col_ind,mat[i,col_ind]))
    else:
        for i in xrange(len(col_mins)):
            row_ind = col_mins[i]
            if mat[row_ind,i] < dist_thresh:
                matches.append((row_ind,i,mat[row_ind,i]))

    return sorted(matches,key= lambda x: x[2])

def two_vote(points1,points2,dist_thresh):

    results = []
    
    n = len(points1)
    m = len(points2)

    diff = n-m
        
    if n == 0 or m == 0:
        results = []
        results.extend(points1)
        results.extend(points2)
        return results

    dist_mat = spatial.distance_matrix([pt.coord for pt in points1],[pt.coord for pt in points2])
    
    points = find_matches(dist_mat,(n<m),dist_thresh)

    for point in points:

        point1 = points1[point[0]]
        point2 = points2[point[1]]
        
        points1[point[0]] = None
        points2[point[1]] = None

        if point1 == None:
            results.append(point2)
        elif point2 == None:
            results.append(point1)
        else:
            avg_point = [(point1.coord[0]+point2.coord[0])/2,(point1.coord[1]+point2.coord[1])/2]
        
            results.append(my_point(avg_point,point1.num+point2.num))
        

    f_pts1 = filter(lambda x: x is not None,points1)
    f_pts2 = filter(lambda x: x is not None,points2)

    results.extend(f_pts1)
    results.extend(f_pts2)

    return results

def n_vote(expert_data,dist_thresh):

    random.shuffle(expert_data)
        
    first_exp = expert_data.pop()

    while expert_data:
        next_exp = expert_data.pop()
        join_exp = two_vote(first_exp, next_exp,dist_thresh)
        first_exp = join_exp
            
    return first_exp



if __name__=="__main__":
    main()
