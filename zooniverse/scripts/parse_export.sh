#!/bin/bash

# grep ',12.57,' exports/microscopy-masters-classifications.csv > raw_data/picker_init.csv
# grep '\[986\]' exports/microscopy-masters-subjects.csv | cut -d',' -f1 | sed 's/^/;/' > raw_data/micromaster_ids.txt
# grep -f raw_data/micromaster_ids.txt raw_data/picker_init.csv > raw_data/picker_data.csv
# rm raw_data/picker_init.csv
# rm raw_data/micromaster_ids.txt

grep 'HIV Trimer' /Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/exports/microscopy-masters-classifications.csv >> /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/raw_data/trimer_data.csv

cut -d',' -f8 /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/raw_data/trimer_data.csv | sort >  /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/raw_data/trimer_times.txt
cut -d',' -f1,2,8 /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/raw_data/trimer_data.csv | sort -k 2,2 -t',' > /Users/Jake/Documents/Projects/crowdProj/HIVTrimer/zooniverse/raw_data/user_times_trimer.csv
